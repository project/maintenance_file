
----- HOW TO USE THIS MODULE ------

Enable it just like you would any other module. When you would like to
put your site in maintenance mode, place a file called "maintenance.lock"
in either the Drupal root directory or your specific site directory
(sites/mysite.com, for instance). The file can be empty if you'd like,
however, if the file has contents, that will be set as the message to
display to users while your site is offline.

When you would like to take your site out of maintenance mode, simply
delete the file.

That's all! Have fun!

